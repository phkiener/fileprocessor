namespace FileProcessor.Application
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFileHandler
    {
        public string Name { get; }
        public IEnumerable<string> Aliases { get; }
        public string Description { get; }
        public string? Parameters { get; set; }

        public Task<IFileProcessingResult> HandleAsync(string filePath);
    }

    public interface IFileProcessingResult
    {
        public string? Message { get; }
    }
}
