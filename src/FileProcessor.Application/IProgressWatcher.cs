namespace FileProcessor.Application
{
    public interface IProgressWatcher
    {
        public void FileProcessed();
        public void SetTotalFilesBeingProcessed(int numberOfFiles);
        public void LogInformation(string line);
    }
}
