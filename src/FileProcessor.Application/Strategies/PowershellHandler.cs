namespace FileProcessor.Application.Strategies
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;

    public sealed class PowershellHandler : IFileHandler
    {
        public string Name => "Powershell";
        public IEnumerable<string> Aliases => new[] { "pwsh" };
        public string Description => "Execute a powershell script on each file. The full path of the current file is available as $File.";
        public string? Parameters { get; set; }

        public async Task<IFileProcessingResult> HandleAsync(string filePath)
        {
            var process = CreatePowershellProcess(filePath);
            return await TryRunProcessAsync(filePath, process);
        }

        private Process CreatePowershellProcess(string filePathToHandle)
        {
            var command = $"& {{ $File = '{filePathToHandle}'; {Parameters}; }}";
            var startInfo = new ProcessStartInfo
            {
                FileName = "pwsh",
                ArgumentList = { "-NoLogo", "-NonInteractive", "-NoProfile", "-Command", command }
            };

            return new Process { StartInfo = startInfo };
        }

        private static async Task<IFileProcessingResult> TryRunProcessAsync(string filePath, Process process)
        {
            try
            {
                var exitCode = await RunProcessAsync(process);
                return IsSuccess(exitCode) ? CreateSuccessResult() : CreateFailureResult(filePath);
            }
            catch (Exception e)
            {
                return CreateExceptionResult(filePath, e);
            }
        }

        private static async Task<int> RunProcessAsync(Process process)
        {
            process.Start();
            await process.WaitForExitAsync();

            return process.ExitCode;
        }

        private static bool IsSuccess(int exitCode)
        {
            return exitCode == 0;
        }

        private static Result CreateSuccessResult()
        {
            return new Result();
        }

        private static Result CreateFailureResult(string filePath)
        {
            return new Result { Message = $"Error handling '{filePath}'" };
        }

        private static IFileProcessingResult CreateExceptionResult(string filePath, Exception exception)
        {
            return new Result { Message = $"Error handling '{filePath}': {exception}" };
        }

        private sealed record Result : IFileProcessingResult
        {
            public string? Message { get; init; }
        }
    }
}
