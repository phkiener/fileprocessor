namespace FileProcessor.Application.Strategies
{
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    public sealed class WindowsToUnixLineEndingsHandler : IFileHandler
    {
        private const string WindowsLineEnding = "\r\n";
        private const string UnixLineEnding = "\n";

        public string Name => "WindowsToUnixLineEndings";
        public IEnumerable<string> Aliases => new[] { "crlf2lf", "dos2unix" };
        public string Description => "Convert Windows line endings (CRLF) to unix file endings (LF)";
        public string? Parameters { get; set; }

        public async Task<IFileProcessingResult> HandleAsync(string filePath)
        {
            var content = await File.ReadAllTextAsync(filePath);
            if (content.Contains(WindowsLineEnding))
            {
                var replacedContent = content.Replace(WindowsLineEnding, UnixLineEnding);
                await File.WriteAllTextAsync(filePath, replacedContent);

                return Result.Success(filePath);
            }

            return Result.Skipped();
        }

        private sealed class Result : IFileProcessingResult
        {
            private Result(string? message)
            {
                Message = message;
            }

            public string? Message { get; }

            public static Result Success(string path)
            {
                return new Result($"Converted file {path}");
            }

            public static Result Skipped()
            {
                return new Result(null);
            }
        }
    }
}
