namespace FileProcessor.Application
{
    using System.Collections.Generic;

    public interface IFileSearch
    {
        public IEnumerable<string> EnumerateFiles(string path);
    }
}
