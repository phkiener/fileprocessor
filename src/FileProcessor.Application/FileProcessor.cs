namespace FileProcessor.Application
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Threading.Tasks.Dataflow;

    public sealed record FileProcessingOptions(IFileHandler FileHandler, int DegreeOfParallelism, CancellationTokenSource CancellationTokenSource);

    public sealed class FileProcessor
    {
        private readonly IFileSearch fileSearch;
        private readonly IProgressWatcher progressWatcher;
        private readonly IPropagatorBlock<string, IFileProcessingResult> processingBlock;
        private readonly ITargetBlock<IFileProcessingResult> targetBlock;

        public FileProcessor(FileProcessingOptions fileProcessingOptions, IFileSearch fileSearch, IProgressWatcher progressWatcher)
        {
            this.progressWatcher = progressWatcher;
            this.fileSearch = fileSearch;

            processingBlock = CreateProcessingBlock(fileProcessingOptions);
            targetBlock = CreateLoggingBlock(progressWatcher);

            processingBlock.LinkTo(targetBlock, new DataflowLinkOptions { PropagateCompletion = true });
        }

        public async Task RunAsync(string path)
        {
            var files = fileSearch.EnumerateFiles(path);
            SendFilesToProcessing(files);

            try
            {
                await Task.WhenAll(processingBlock.Completion, targetBlock.Completion);
            }
            catch (TaskCanceledException)
            {
                progressWatcher.LogInformation($"{Environment.NewLine}--- Canceled");
            }
        }

        private void SendFilesToProcessing(IEnumerable<string> files)
        {
            var numberOfFiles = 0;
            foreach (var f in files)
            {
                numberOfFiles += 1;

                processingBlock.Post(f);
                progressWatcher.SetTotalFilesBeingProcessed(numberOfFiles);
            }

            processingBlock.Complete();
        }

        private static TransformBlock<string, IFileProcessingResult> CreateProcessingBlock(FileProcessingOptions fileProcessingOptions)
        {
            var blockOptions = new ExecutionDataflowBlockOptions
            {
                CancellationToken = fileProcessingOptions.CancellationTokenSource.Token,
                SingleProducerConstrained = true,
                MaxDegreeOfParallelism = fileProcessingOptions.DegreeOfParallelism
            };

            return new TransformBlock<string, IFileProcessingResult>(fileProcessingOptions.FileHandler.HandleAsync, blockOptions);
        }

        private static ActionBlock<IFileProcessingResult> CreateLoggingBlock(IProgressWatcher progressWatcher)
        {
            var blockOptions = new ExecutionDataflowBlockOptions
            {
                CancellationToken = CancellationToken.None,
                SingleProducerConstrained = false,
                MaxDegreeOfParallelism = 1
            };

            return new ActionBlock<IFileProcessingResult>(r => LogResult(r, progressWatcher), blockOptions);
        }

        private static void LogResult(IFileProcessingResult result, IProgressWatcher progressWatcher)
        {
            progressWatcher.FileProcessed();

            if (result.Message is not null)
            {
                progressWatcher.LogInformation(result.Message);
            }
        }
    }
}
