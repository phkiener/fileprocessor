namespace FileProcessor.Cli
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application;
    using CliFx;
    using CliFx.Attributes;
    using FileSearch;

    [Command(Description = "Process all files in directory using a certain strategy")]
    internal sealed class Process : ICommand
    {
        [CommandOption("recurse", 'r', Description = "Whether to recurse into subdirectories")]
        public bool Recurse { get; set; } = false;

        [CommandOption("num-threads", 'n', Description = "How many threads to use for file processing")]
        public int NumberOfThreads { get; set; } = 1;

        [CommandOption("include", Description = "Patterns for files to match in order to be processed; if none are given, all files are included")]
        public string[] Include { get; set; } = Array.Empty<string>();

        [CommandOption("exclude", Description = "Patterns for file that must not be processed; if none are given, no files are excluded")]
        public string[] Exclude { get; set; } = Array.Empty<string>();

        [CommandOption("params", 'p', Description = "Additional parameters for the strategy")]
        public string? Parameters { get; set; } = null;

        [CommandParameter(0, Description = "Strategy to use")]
        public string Strategy { get; set; } = null!;

        [CommandParameter(1, Description = "Path to process")]
        public string Path { get; set; } = null!;

        public async ValueTask ExecuteAsync(IConsole console)
        {
            var fileHandler = GetCalledFileHandler();
            fileHandler.Parameters = Parameters;

            var fileSearch = new FilteredBreadthFirstSearch(Include, Exclude, Recurse);

            var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(console.GetCancellationToken());
            var processingOptions = new FileProcessingOptions(fileHandler, NumberOfThreads, cancellationTokenSource);
            var progressWatcher = new ConsoleProgressWatcher(console);

            var processor = new FileProcessor(processingOptions, fileSearch, progressWatcher);

            await processor.RunAsync(Path);
        }

        private IFileHandler GetCalledFileHandler()
        {
            var fileHandler = Program.FileHandlers.SingleOrDefault(h => h.Name == Strategy || h.Aliases.Any(a => a == Strategy));
            if (fileHandler is not null)
            {
                return fileHandler;
            }

            throw new KeyNotFoundException($"Strategy '{Strategy}' was not found");
        }
    }
}
