﻿namespace FileProcessor.Cli
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Application;
    using CliFx;

    internal static class Program
    {
        internal static readonly IReadOnlyCollection<IFileHandler> FileHandlers = CollectFileHandlers();

        private static async Task Main(string[] args)
        {
            var application = new CliApplicationBuilder()
                .AddCommand<Process>()
                .AddCommand<ListStrategies>()
                .UseTitle(nameof(FileProcessor))
                .UseDescription("A general-purpose file processor to run a script on all files in a directory")
                .UseVersionText("")
                .Build();

            await application.RunAsync(args);
        }

        private static IReadOnlyCollection<IFileHandler> CollectFileHandlers()
        {
            var assembly = Assembly.Load("FileProcessor.Application");
            var types = assembly.GetTypes()
                .Where(IsFileHandler)
                .ToList();

            return types.Select(t => Activator.CreateInstance(t) as IFileHandler).ToList()!;
        }

        private static bool IsFileHandler(Type type)
        {
            return type.IsClass
                   && type.IsPublic
                   && type.IsAssignableTo(typeof(IFileHandler))
                   && type.GetConstructors().Any(c => c.GetParameters().Length == 0);
        }
    }
}
