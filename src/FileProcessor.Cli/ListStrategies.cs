namespace FileProcessor.Cli
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Application;
    using CliFx;
    using CliFx.Attributes;

    [Command("strategies", Description = "List all available strategies")]
    internal sealed class ListStrategies : ICommand
    {
        public async ValueTask ExecuteAsync(IConsole console)
        {
            const int maxWidth = 79;
            const int columnSpacing = 4;
            var maximumNameWidth = Program.FileHandlers.Select(fh => fh.Name.Length).Max();
            var maximumDescriptionWidth = maxWidth - (maximumNameWidth + columnSpacing);

            foreach (var handler in Program.FileHandlers)
            {
                var handlerDescription = DescribeFileHandler(handler, maximumNameWidth + columnSpacing, maximumDescriptionWidth);
                await console.Output.WriteLineAsync(handlerDescription);
            }
        }

        private static string DescribeFileHandler(IFileHandler fileHandler, int nameColumnWidth, int descriptionColumnWidth)
        {
            var paddedName = fileHandler.Name.PadRight(nameColumnWidth);
            var paddedAliases = fileHandler.Aliases.Select(a => $"> {a}".PadRight(nameColumnWidth)).ToList();

            var nameColumnEntries = paddedAliases.Prepend(paddedName).ToList();
            var descriptionColumnEntries = Split(fileHandler.Description, descriptionColumnWidth).ToList();

            var handlerDescriptionBuilder = new StringBuilder();
            for (var i = 0; i < Math.Max(nameColumnEntries.Count, descriptionColumnEntries.Count); ++i)
            {
                var nameCell = nameColumnEntries.ElementAtOrDefault(i) ?? new string(' ', nameColumnWidth);
                var descriptionCell = descriptionColumnEntries.ElementAtOrDefault(i) ?? "";

                handlerDescriptionBuilder.AppendLine(nameCell + descriptionCell);
            }

            return handlerDescriptionBuilder.ToString();
        }

        private static IEnumerable<string> Split(string source, int partSize)
        {
            var currentLineBuilder = new StringBuilder();
            foreach (var word in source.Split(' ', '\t', '\n'))
            {
                if (currentLineBuilder.Length + 1 + word.Length > partSize)
                {
                    yield return currentLineBuilder.ToString();

                    currentLineBuilder.Clear();
                }

                if (currentLineBuilder.Length > 0)
                {
                    currentLineBuilder.Append(' ');
                }

                currentLineBuilder.Append(word);
            }

            if (currentLineBuilder.Length > 0)
            {
                yield return currentLineBuilder.ToString();
            }
        }
    }
}
