namespace FileProcessor.Cli
{
    using System.IO;
    using Application;
    using CliFx;

    internal sealed class ConsoleProgressWatcher : IProgressWatcher
    {
        private readonly TextWriter outputWriter;
        private int filesProcessed;

        public ConsoleProgressWatcher(IConsole console)
        {
            outputWriter = console.Output;
        }

        public int FilesProcessed => filesProcessed;

        public void FileProcessed()
        {
            filesProcessed += 1;
        }

        public void SetTotalFilesBeingProcessed(int numberOfFiles)
        {
        }

        public void LogInformation(string line)
        {
            outputWriter.WriteLine(line);
        }
    }
}
