namespace FileProcessor.Cli.FileSearch
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Application;

    public sealed class FilteredBreadthFirstSearch : IFileSearch
    {
        private readonly IReadOnlyCollection<Regex> excludeFileFilters;
        private readonly IReadOnlyCollection<Regex> includeFileFilters;
        private readonly bool recurseIntoDirectories;

        public FilteredBreadthFirstSearch(IEnumerable<string> includeFilters, IEnumerable<string> excludeFilters, bool recurseIntoDirectories)
        {
            includeFileFilters = includeFilters.Select(CreateRegexFilter).ToList();
            excludeFileFilters = excludeFilters.Select(CreateRegexFilter).ToList();
            this.recurseIntoDirectories = recurseIntoDirectories;
        }

        public IEnumerable<string> EnumerateFiles(string path)
        {
            var directoriesToProcess = new Queue<string>(new[] { Path.GetFullPath(path) });
            var includeAll = includeFileFilters.Count == 0;

            while (directoriesToProcess.TryDequeue(out var dir))
            {
                foreach (var file in Directory.EnumerateFiles(dir))
                {
                    var relativePath = Path.GetRelativePath(path, file);
                    var includeFile = includeAll || includeFileFilters.Any(regex => regex.IsMatch(relativePath));
                    var excludeFile = excludeFileFilters.Any(regex => regex.IsMatch(relativePath));

                    if (includeFile && !excludeFile)
                    {
                        yield return file;
                    }
                }

                if (recurseIntoDirectories)
                {
                    var directories = Directory.EnumerateDirectories(dir);
                    foreach (var directory in directories)
                    {
                        directoriesToProcess.Enqueue(directory);
                    }
                }
            }
        }

        private static Regex CreateRegexFilter(string wildcardPattern)
        {
            var escapedPattern = Regex.Escape(wildcardPattern.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar));
            return new Regex($"^{escapedPattern.Replace("\\*", ".*")}$");
        }
    }
}
