namespace FileProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application;
    using Cli.FileSearch;
    using NUnit.Framework;

    [TestFixture]
    internal sealed class FileProcessorTest : WithTestFileSystem
    {
        [SetUp]
        public void SetUp()
        {
            testFileHandler = new TestFileHandler();
        }

        private TestFileHandler testFileHandler = null!;

        [Test]
        public void Run_RecurseWithoutFilters_AllFilesAreProcessed()
        {
            // Arrange
            var files = new[] { "file.txt", "dir1/file.txt", "dir1/dir2/file.txt" };
            WithFiles(files);

            // Act
            Execute(recurse: true);

            // Assert
            AssertFilesAreProcessed(files);
        }

        [Test]
        public void Run_NoRecurseWithoutFilters_TopLevelFilesAreProcessed()
        {
            // Arrange
            var files = new[] { "file.txt", "dir1/file.txt", "dir1/dir2/file.txt" };
            WithFiles(files);

            // Act
            Execute(recurse: false);

            // Assert
            AssertFilesAreProcessed("file.txt");
        }

        [Test]
        public void Run_WithIncludes_ProcessesOnlyMatchingFiles()
        {
            // Arrange
            var files = new[] { "file.txt", "file.bin", "file.txt.bin" };
            WithFiles(files);

            // Act
            Execute(recurse: false, includes: new[] { "*.txt" });

            // Assert
            AssertFilesAreProcessed("file.txt");
        }

        [Test]
        public void Run_WithExcludes_DoesNotProcessMatchingFiles()
        {
            // Arrange
            var files = new[] { "file.txt", "dir1/file.txt", "dir2/file.txt" };
            WithFiles(files);

            // Act
            Execute(recurse: true, excludes: new[] { "dir1/*" });

            // Assert
            AssertFilesAreProcessed("file.txt", "dir2/file.txt");
        }

        [Test]
        public void Run_WitIncludeAndExclude_ProcessesCorrectFile()
        {
            // Arrange
            var files = new[] { "file.txt", "nope/file.txt" };
            WithFiles(files);

            // Act
            Execute(recurse: false, includes: new[] { "*.txt" }, excludes: new[] { "nope/*" });

            // Assert
            AssertFilesAreProcessed("file.txt");
        }

        private void Execute(bool recurse, IReadOnlyCollection<string>? includes = null, IReadOnlyCollection<string>? excludes = null)
        {
            var fileSearch = new FilteredBreadthFirstSearch(includes ?? Array.Empty<string>(), excludes ?? Array.Empty<string>(), recurse);
            var processingOptions = new FileProcessingOptions(testFileHandler, 1, new CancellationTokenSource());
            var fileProcessor = new FileProcessor(processingOptions, fileSearch, new TestProgressWatcher());

            fileProcessor.RunAsync(path: GetRoot()).GetAwaiter().GetResult();
        }

        private void WithFiles(params string[] fileNames)
        {
            foreach (var fileName in fileNames)
            {
                WithFile(fileName);
            }
        }

        private void AssertFilesAreProcessed(params string[] fileNames)
        {
            var relativePaths = testFileHandler.HandledFiles.Select(RelativePath).ToList();
            var actualRelativePaths = fileNames.Select(EnsureCorrectPathSeparator).ToList();

            Assert.That(relativePaths, Is.EquivalentTo(actualRelativePaths));
        }

        private static string EnsureCorrectPathSeparator(string path)
        {
            return Path.Join(path.Split(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar));
        }

        private sealed class TestFileHandler : IFileHandler
        {
            private readonly ICollection<string> handledFiles = new List<string>();
            public IEnumerable<string> HandledFiles => handledFiles.ToList();

            public string Name => "Test";
            public IEnumerable<string> Aliases => Array.Empty<string>();
            public string Description => "Test file handler";
            public string? Parameters { get; set; }

            public Task<IFileProcessingResult> HandleAsync(string filePath)
            {
                handledFiles.Add(filePath);
                return Task.FromResult<IFileProcessingResult>(new TestResult());
            }
        }

        private sealed class TestProgressWatcher : IProgressWatcher
        {
            public void FileProcessed()
            { }

            public void SetTotalFilesBeingProcessed(int numberOfFiles)
            { }

            public void LogInformation(string line)
            { }
        }

        private sealed class TestResult : IFileProcessingResult
        {
            public string? Message => null;
        }
    }
}
