namespace FileProcessor
{
    using System;
    using System.IO;

    internal abstract class WithTestFileSystem : IDisposable
    {
        private readonly string fileSystemPath;

        protected WithTestFileSystem()
        {
            var testRunId = $"{GetType().Name}-{Guid.NewGuid():D}";
            fileSystemPath = Path.Combine(Path.GetTempPath(), testRunId);

            Directory.CreateDirectory(fileSystemPath);
        }

        public void Dispose()
        {
            Directory.Delete(fileSystemPath, recursive: true);
        }

        public void WithFile(string path, string content = "")
        {
            var fullPath = Path.Combine(fileSystemPath, path);
            var directory = fullPath.Replace(Path.GetFileName(path), string.Empty);

            Directory.CreateDirectory(directory);
            using var stream = File.CreateText(fullPath);
            stream.Write(content);
        }

        public string GetContent(string path)
        {
            var fullPath = Path.Combine(fileSystemPath, path);
            return File.ReadAllText(fullPath);
        }

        public string RelativePath(string absolutePath)
        {
            return Path.GetRelativePath(fileSystemPath, absolutePath);
        }

        public string GetRoot()
        {
            return fileSystemPath;
        }
    }
}
