namespace FileProcessor.Strategies
{
    using System.IO;
    using Application;
    using Application.Strategies;
    using NUnit.Framework;

    [TestFixture]
    internal sealed class WindowsToUnixLineEndingsHandlerTest : WithTestFileSystem
    {
        [SetUp]
        public void SetUp()
        {
            windowsToUnixLineEndingsHandler = new WindowsToUnixLineEndingsHandler();
            result = null!;
        }

        private WindowsToUnixLineEndingsHandler windowsToUnixLineEndingsHandler = null!;
        private const string FileName = "someFile.txt";
        private IFileProcessingResult result = null!;

        [Test]
        public void HandleAsync_FileContainsCrlf_ReplacedWithLf()
        {
            // Arrange
            WithFile("Bad" + "\r\n" + "Lines" + "\r\n");

            // Act
            Execute();

            // Assert
            AssertResultMessageContainsFileName();
            AssertFileContains("Bad" + "\n" + "Lines" + "\n");
        }

        [Test]
        public void HandleAsync_FileContainsLf_NothingHappens()
        {
            // Arrange
            WithFile("Good" + "\n" + "Lines" + "\n");

            // Act
            Execute();

            // Assert
            AssertResultMessageIsNull();
            AssertFileContains("Good" + "\n" + "Lines" + "\n");
        }

        private void WithFile(string content)
        {
            WithFile(FileName, content);
        }

        private void Execute()
        {
            result = windowsToUnixLineEndingsHandler.HandleAsync(Path.Combine(GetRoot(), FileName)).GetAwaiter().GetResult();
        }

        private void AssertResultMessageIsNull()
        {
            Assert.That(result.Message, Is.Null);
        }

        private void AssertResultMessageContainsFileName()
        {
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(result.Message, Does.Contain(FileName));
        }

        private void AssertFileContains(string content)
        {
            var actualContent = GetContent(FileName);
            Assert.That(actualContent, Is.EqualTo(content));
        }
    }
}
