param([Parameter(Mandatory=$false)][string]$toolName = "fileprocessor")

$tempDir = Join-Path ([System.IO.Path]::GetTempPath()) "packages"
$packageVersion="69.69.69-custom"

dotnet build -c Release (Join-Path src FileProcessor FileProcessor.csproj) -p:PackAsTool=true -p:ToolCommandName=$toolName -p:PackageVersion=$packageVersion
dotnet pack -c Release (Join-Path src FileProcessor FileProcessor.csproj)  --no-build --output=$tempDir -p:PackAsTool=true -p:ToolCommandName=$toolName -p:PackageVersion=$packageVersion

dotnet tool uninstall -g FileProcessor
dotnet tool install -g FileProcessor --add-source $tempDir --version $packageVersion
Remove-Item -Recurse $tempDir

