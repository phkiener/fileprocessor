# FileProcessor

An unambitious but "general-purpose" file processor.
It will step through a directory, recursively if needed, and process all files matching given filters.

The processing is done by a `IFileHandler` defined in the code itself.

## Installation

There is a powershell script provided (`Install.ps1`) that will install `FileProcessor` as a global dotnet tool. Just run it and you're good to go! Beware though; if you've already got a tool with the name 'fileprocessor' installed, it will be removed.

### Fine print

It basically works by creating a package (with version 69.69.69-custom) and installing it by adding the temp folder it is created in as nuget source.
There is an actual nuget package called FileProcessor (which is not a tool), causing a conflict - hence the fancy version.

You can override the tool name by passing it into the script; default is `fileprocessor`.

## Usage

Run `fileprocessor --help` to see how to use it.

## Extending - adding your own strategy

To add your own strategy, you need only implement the `IFileHandler` interface within the assembly as a public class. Everything else will be autowired!
If you need additional settings for your strategy, you can parse the `Parameters` property - everything passed to the `-p` option will be in there.
